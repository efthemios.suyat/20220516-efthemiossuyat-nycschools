# 20220516-EfthemiosSuyat-NYCSchools
JPMC - iOS Coding Challenge: NYC Schools

## Requirements 
- iOS 15.0+ Used new concurrency feature async/await
- Swift 5.5+

## To BUILD and RUN:
1. Download [XCode](https://developer.apple.com/xcode/download/) 13 release.
2. Clone this repository.

## Notes
1. The app was built using MVVM architecture.
2. Web services are encapsulated in the `SchoolService` type
4. `SchoolListViewModel` responsible for loading data and notifying `ViewController`