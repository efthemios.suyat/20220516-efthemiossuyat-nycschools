//
//  UITableViewCell.swift
//  20220516-EfthemiosSuyat-NYCSchools
//
//  Created by Bong Suyat on 5/17/22.
//

import UIKit

protocol ReusableCell {
  static var reuseIdentifier: String { get }
}

extension ReusableCell {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
}

extension UITableViewCell: ReusableCell {}
