//
//  SchoolListCell.swift
//  20220516-EfthemiosSuyat-NYCSchools
//
//  Created by Bong Suyat on 5/17/22.
//

import UIKit
import MapKit
import CoreLocation
import CoreMotion

class SchoolListCell: UITableViewCell {

  @IBOutlet weak var nameLabel: UILabel!
  
  var schoolModel: SchoolModel? {
    didSet {
      self.nameLabel.text = schoolModel?.name
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    self.nameLabel.text = nil
  }
  
  
  @IBAction func wwwTap(_ sender: Any) {
    if let url = URL(string: "https://\(schoolModel!.www)"), UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url)
    }
  }
  
  @IBAction func emailTap(_ sender: Any) {
    guard let email = schoolModel!.email else {
      return
    }
    
    if let url = URL(string: "mailto:\(email)"), UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url)
    }

  }
  
  @IBAction func telTap(_ sender: Any) {
    if let url = URL(string: "tel:\(schoolModel!.phone)"), UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url)
    }
  }

  @IBAction func mapTap(_ sender: Any) {
    guard let lat = schoolModel?.lat, let long = schoolModel?.long else {
      return
    }

    let coordinate = CLLocationCoordinate2DMake(Double(lat)!, Double(long)!)
    let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
    mapItem.name = schoolModel?.name
    mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
  }
}
