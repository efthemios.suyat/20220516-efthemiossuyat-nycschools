//
//  ScoreModel.swift
//  20220516-EfthemiosSuyat-NYCSchools
//
//  Created by Bong Suyat on 5/17/22.
//

import Foundation

struct ScoreModel: Decodable {
  let dbn: String
  let takers: String
  let reading: String
  let math: String
  let writing: String
  
  private enum CodingKeys: String, CodingKey {
    case dbn = "dbn"
    case takers = "num_of_sat_test_takers"
    case reading = "sat_critical_reading_avg_score"
    case math = "sat_math_avg_score"
    case writing = "sat_writing_avg_score"
  }
}
