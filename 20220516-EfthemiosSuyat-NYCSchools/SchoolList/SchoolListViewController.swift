//
//  SchoolListViewController.swift
//  20220516-EfthemiosSuyat-NYCSchools
//
//  Created by Bong Suyat on 5/17/22.
//

import UIKit

class SchoolListViewController: UIViewController, UITableViewDelegate {


  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  private let service = SchoolService()
  var schoolList = [SchoolModel]()
  var scoreList = [ScoreModel]()
  private var viewModel = SchoolListViewModel(service: SchoolService())
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = "NYC High Schools"

    setActivityIndicator()
    setupTableView()
    loadSchoolList()
    
  }
  
  
  private func setupTableView() {
    tableView.delegate = self
    tableView.dataSource = self
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 44
  }
  
  private func setActivityIndicator() {
    self.activityIndicator.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
    self.activityIndicator.hidesWhenStopped = true
  }
  
  
  func loadSchoolList() {
    
    viewModel.didUpdate = { [weak self] in
      switch self?.viewModel.state {
      case .loading:
        DispatchQueue.main.async {
          self?.activityIndicator.startAnimating()
        }
      case .success(let schoolData, let scoreData):
        self?.schoolList = schoolData
        self?.scoreList = scoreData
        DispatchQueue.main.async {
          self?.activityIndicator.stopAnimating()
          self?.tableView.reloadData()
        }

      case .failed(let error):
        print("\(error.localizedDescription)")
      default:
        DispatchQueue.main.async {
          self?.activityIndicator.stopAnimating()

        }
      }
    }

    
    Task {
      await viewModel.getShoolList()
    }
  }
  

}


extension SchoolListViewController: UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.schoolList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let schoolCell: SchoolListCell = tableView.dequeueReusableCell(for: indexPath)
    schoolCell.schoolModel = self.schoolList[indexPath.row]
    return schoolCell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let schoolDetail = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SchoolDetailViewController") as! SchoolDetailViewController
    
    schoolDetail.navigationItem.title = self.schoolList[indexPath.row].name
    
    let currentSchoolData = self.schoolList[indexPath.row]
    schoolDetail.schoolData = currentSchoolData
    
    if let score = self.scoreList.first(where: {$0.dbn == currentSchoolData.dbn}) {
      schoolDetail.schoolData?.score = score
    }

    self.navigationController?.pushViewController(schoolDetail, animated: true)
  }

}
