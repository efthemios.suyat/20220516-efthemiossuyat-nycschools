//
//  SchoolListViewModel.swift
//  20220516-EfthemiosSuyat-NYCSchools
//
//  Created by Bong Suyat on 5/18/22.
//

import Foundation

class SchoolListViewModel {
  
  enum State {
    case na
    case loading
    case success(schoolData: [SchoolModel], scoreData: [ScoreModel])
    case failed(error: Error)
  }
  
  private(set) var state: State = .na
  
  private let service: SchoolService
  
  var didUpdate: (()->())? = nil
  
  init(service: SchoolService) {
    self.service = service
  }
  
  func getShoolList() async {
    self.state = .loading
    didUpdate?()

    async let schools = service.fetch(.schools, type: SchoolModel.self)
    async let scores = service.fetch(.scores, type: ScoreModel.self)
    
    do {
      let (schoolList, scoreList) = try await (schools, scores)
      self.state = .success(schoolData: schoolList, scoreData: scoreList)
      didUpdate?()
    } catch {
      self.state = .failed(error: error)
      didUpdate?()
    }
  }
}
