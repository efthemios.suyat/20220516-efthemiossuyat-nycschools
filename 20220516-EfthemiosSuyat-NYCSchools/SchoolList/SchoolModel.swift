//
//  SchoolModel.swift
//  20220516-EfthemiosSuyat-NYCSchools
//
//  Created by Bong Suyat on 5/17/22.
//

import Foundation

struct SchoolModel: Decodable {
  let dbn: String
  let name: String
  let overview: String
  let www: String
  let phone: String
  let address: String
  let city: String
  let state: String
  let zip: String
  let totalStudents: String
  let email: String?
  let lat: String?
  let long: String?
  var score: ScoreModel?
  
  private enum CodingKeys: String, CodingKey {
    case dbn = "dbn"
    case name = "school_name"
    case overview = "overview_paragraph"
    case www = "website"
    case email = "school_email"
    case phone = "phone_number"
    case address = "primary_address_line_1"
    case city = "city"
    case state = "state_code"
    case zip = "zip"
    case lat = "latitude"
    case long = "longitude"
    case totalStudents = "total_students"
  }
}
