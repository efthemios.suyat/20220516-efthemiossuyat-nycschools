//
//  SchoolAPI.swift
//  20220516-EfthemiosSuyat-NYCSchools
//
//  Created by Bong Suyat on 5/17/22.
//

import Foundation

enum SchoolRequest {
  case schools
  case scores
}

extension SchoolRequest {
  var url: URL {
    switch self {
    case .schools:
      return URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
    case .scores:
      return URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!
    }
  }
}

struct SchoolService {
  
  enum SchoolServiceError: Error {
    case requestFailed(description: String)
    case failedToDecode(description: String)
    case responseUnsuccessful(description: String)
    case invalidData
    case noInternet
  }
  
  func fetch<T: Decodable>(_ request: SchoolRequest, type: T.Type) async throws -> [T] {
    let (data, response) = try await URLSession.shared.data(from: request.url)
    guard let httpResponse = response as? HTTPURLResponse else {
      throw SchoolServiceError.requestFailed(description: "Invalid response")
    }
    
    guard httpResponse.statusCode == 200 else {
      throw SchoolServiceError.responseUnsuccessful(description: "status code \(httpResponse.statusCode)")
    }
    
    let result = try JSONDecoder().decode([T].self, from: data)
    return result
  }
}
