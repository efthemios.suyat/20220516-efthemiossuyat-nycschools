//
//  SchoolDetailViewController.swift
//  20220516-EfthemiosSuyat-NYCSchools
//
//  Created by Bong Suyat on 5/18/22.
//

import UIKit

class SchoolDetailViewController: UIViewController {

  @IBOutlet weak var overviewLabel: UILabel!
  
  @IBOutlet weak var addressLabel: UILabel!
  
  @IBOutlet weak var satNumberOfStudents: UILabel!
  
  @IBOutlet weak var satTakersLabel: UILabel!
  
  @IBOutlet weak var satReadingLabel: UILabel!
  
  @IBOutlet weak var satMathLabel: UILabel!
  
  @IBOutlet weak var satWritingLabel: UILabel!
  
  var schoolData: SchoolModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()

    if let schoolData = schoolData {
      overviewLabel.text = schoolData.overview
      addressLabel.text = """
      \(schoolData.address)
      \(schoolData.city) \(schoolData.state) \(schoolData.zip)
      
      Email: \(schoolData.email ?? "N/A")
      Phone: \(schoolData.phone)
      Webiste: \(schoolData.www)
      """.trimmingCharacters(in: .whitespacesAndNewlines)
      
      if let score = schoolData.score {
        satNumberOfStudents.text = schoolData.totalStudents
        satTakersLabel.text = score.takers
        satReadingLabel.text = score.reading
        satMathLabel.text = score.math
        satWritingLabel.text = score.writing
      }

    }
  }
    

}
